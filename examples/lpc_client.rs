use lpc::types::{IpcExt, LpcExt};
use once_cell::sync::Lazy;

/*
windows test data:
client send(is_res: false) sum: 127197
client recver(is_res: true) sum: 7945

linux test data:
client send(is_res: false) sum: 402645
client recver(is_res: true) sum: 11032
如上所示, linux debug 模式下, 单向并发达到了40万

*/
fn main() {
    common_uu::log4rs_mod::init().unwrap();
    let client = lpc::client::LpcClient::new("./Server_key", 1).unwrap();
    client.recver(|_data, call| {
        if let Some(call) = call {
            call.response(b"1231gfdgdsgd45342312".to_vec()).unwrap();
        }

        // 测试并发性能的代码, 生产环境不要使用
        // test bench code; production is not need
        static RECKON: once_cell::sync::Lazy<fast_able::statis::Statis> =
            once_cell::sync::Lazy::new(|| {
                fast_able::statis::Statis::new(|v| println!("client recver sum: {v}"))
            });
        RECKON.add();
    });

    loop {
        if let Err(e) = client.send_res("1231fdertrrdgdfg2312") {
            eprintln!("client.send: {e:?}");
            std::thread::sleep(std::time::Duration::from_millis(1 * 1000));
            continue;
        }

        // 测试并发性能的代码, 生产环境不要使用
        // test bench code; production is not need
        static RECKON: once_cell::sync::Lazy<fast_able::statis::Statis> =
            once_cell::sync::Lazy::new(|| {
                fast_able::statis::Statis::new(|v| println!("client send(is_res: true) sum: {v}"))
            });
        RECKON.add();
    }
}


pub static LPC_CLIENT: Lazy<lpc::client::LpcClient> = Lazy::new(|| {
    lpc::client::LpcClient::new("./Server_key", 5).expect("Failed to start lpc server")
});