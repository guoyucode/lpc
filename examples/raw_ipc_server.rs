use std::{thread, time::Duration};

use ipc_channel::platform::{
    self, OsIpcChannel, OsIpcOneShotServer, OsIpcReceiverSet, OsIpcSender,
};

fn main() {
    let (server, name) = OsIpcOneShotServer::new().unwrap();
    println!("Server key: {name}");
    std::fs::write("./Server_key", name).unwrap();
    // Wait until the child is ready
    let (server, res, mut channels, _) = server.accept().unwrap();
    assert!(res == b" Ready! ");
    let tx1 = channels.first_mut().unwrap().to_sender();
    tx1.send(b"Test", vec![], vec![]).unwrap();
    // The interrupt shouldn't affect the following send
    let (res, _, _) = server.recv().unwrap();
    assert!(res == b"Success!");

    // let rt = tokio::runtime::Builder::new_multi_thread().enable_all().build().unwrap();
    loop {
        // let t = std::time::SystemTime::now();
        tx1.send(b"Test", vec![], vec![]).unwrap();
        // println!("elapsed: {:?}", t.elapsed().unwrap());
        RECKON_SERVER.add();
        // let j1 = rt.spawn_blocking(move || {
        //     tx.send(b"Test spawn_blocking1", vec![], vec![]).unwrap();
        //     RECKON_SERVER.add();
        // });
        // let tx = tx1.clone();
        // let j2 = rt.spawn_blocking(move || {
        //     tx.send(b"Test spawn_blocking1", vec![], vec![]).unwrap();
        //     RECKON_SERVER.add();
        // });
        // let tx = tx1.clone();
        // let j3 = rt.spawn_blocking(move || {
        //     tx.send(b"Test spawn_blocking2", vec![], vec![]).unwrap();
        //     RECKON_SERVER.add();
        // });
        // tokio::join!(j1, j2, j3);
    }
}



pub static RECKON_SERVER: once_cell::sync::Lazy<fast_able::statis::Statis> =
    once_cell::sync::Lazy::new(|| {
        fast_able::statis::Statis::new(|v| println!("!!server sec run sum: {v}"))
    });

pub static RECKON_CLIENT: once_cell::sync::Lazy<fast_able::statis::Statis> =
    once_cell::sync::Lazy::new(|| {
        fast_able::statis::Statis::new(|v| println!("client sec run sum: {v}"))
    });
