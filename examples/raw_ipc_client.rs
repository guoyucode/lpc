use std::{thread, time::Duration};

use ipc_channel::platform::{
    self, OsIpcChannel, OsIpcOneShotServer, OsIpcReceiverSet, OsIpcSender,
};


fn main() {
    let (tx0, rx0) = platform::channel().unwrap();
    let mut rx_set = OsIpcReceiverSet::new().unwrap();
    let rx_id = rx_set.add(rx0).unwrap();
    // Let the parent know we're ready
    let key = std::fs::read("./Server_key").unwrap();
    let key = String::from_utf8(key).unwrap();
    let tx1 = OsIpcSender::connect(key).unwrap();
    tx1.send(b" Ready! ", vec![OsIpcChannel::Sender(tx0)], vec![])
        .unwrap();

    // Send the result of the select back to the parent
    let result = rx_set.select().unwrap();
    let mut result_iter = result.into_iter();
    let (id, data, _, _) = result_iter.next().unwrap().unwrap();
    assert_eq!(rx_id, id);
    assert_eq!(b"Test".as_ref(), &*data);
    assert!(result_iter.next().is_none());
    tx1.send(b"Success!", vec![], vec![]).unwrap();

    loop{
        let r = rx_set.select().unwrap();
        for ele in r {
            let (id, data, _, _) = ele.unwrap();
            RECKON_CLIENT.add();
        }
    }
}

pub static RECKON_SERVER: once_cell::sync::Lazy<fast_able::statis::Statis> =
    once_cell::sync::Lazy::new(|| {
        fast_able::statis::Statis::new(|v| println!("!!server sec run sum: {v}"))
    });

pub static RECKON_CLIENT: once_cell::sync::Lazy<fast_able::statis::Statis> =
    once_cell::sync::Lazy::new(|| {
        fast_able::statis::Statis::new(|v| println!("client sec run sum: {v}"))
    });
