use lpc::types::{IpcExt, LpcExt};

fn main() {
    common_uu::log4rs_mod::init().unwrap();
    let server = lpc::server::LpcServer::new("./Server_key");
    server.recver(|_data, call| {
        if let Some(call) = call {
            call.response(b"123135345342312".to_vec()).unwrap();
        }

        // 测试并发性能的代码, 生产环境不要使用
        // test bench code; production is not need
        static RECKON: once_cell::sync::Lazy<fast_able::statis::Statis> =
            once_cell::sync::Lazy::new(|| {
                fast_able::statis::Statis::new(|v| println!("server recver sum: {v}"))
            });
        RECKON.add();
    });

    loop {
        if let Err(e) = server.send("12312312") {
            eprintln!("server.send: {e:?}");
            std::thread::sleep(std::time::Duration::from_millis(1 * 1000));
            continue;
        }

        // 测试并发性能的代码, 生产环境不要使用
        // test bench code; production is not need
        static RECKON: once_cell::sync::Lazy<fast_able::statis::Statis> =
            once_cell::sync::Lazy::new(|| {
                fast_able::statis::Statis::new(|v| println!("server send sum: {v}"))
            });
        RECKON.add();
    }
}
