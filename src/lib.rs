#[macro_use]
extern crate log;

pub mod client;
pub mod server;
pub mod types;
pub mod utils;

type StdBoxError = Box<dyn std::error::Error + Send + Sync>;
pub type R<V = ()> = Result<V, StdBoxError>;

