pub fn sleep_1kms() {
    std::thread::sleep(std::time::Duration::from_millis(1000));
}